## OS Specifics

### Windows

RVGL for Windows is provided in an installer format. The installer sets the necessary 
registry entries and file permissions, and optionally creates a Desktop shortcut.

The Windows versions support DirectPlay lobby applications like GameRanger or 
GameSpy Arcade. Support for this is provided in *dplobby_helper.dll*. Running the setup 
also ensures that the game is properly detected by lobby applications.

> Running RVGL in Administrator mode is not necessary and not recommended.

#### Direct3D Backend

On Windows systems with poor or non-existent OpenGL support, RVGL can take advantage of 
Google's [ANGLE project](https://en.wikipedia.org/wiki/ANGLE_%28software%29) which provides 
a compliant OpenGL ES implementation based on Direct3D9 or Direct3D11. To use this support, 
edit *rvgl.ini* and set `Shaders = 1` to enable the shader-based renderer, then set 
`Profile = 3`.

You can verify whether the Direct3D backend is being used by checking the video info 
in *profiles\\re-volt.log*. Below is a sample output:
```
GL Vendor: Google Inc.
GL Renderer: ANGLE (AMD Radeon™ R4 Graphics Direct3D11 vs_5_0 ps_5_0)
GL Version: OpenGL ES 2.0 (ANGLE 2.1.0.54118be67360)
```

#### Discord URI

RVGL registers the custom URI `discord-472158403830218762://` for Discord Rich Presence 
integration. The registration is done only once during the RVGL setup. To customize the 
URI, eg., to set various launch parameters, edit the registry key at:
```
HKEY_CURRENT_USER\Software\Classes\discord-472158403830218762\shell\open\command
```
The `"%1"` at the end of the path is a required parameter. The default URI can be 
re-registered by running RVGL with the `-register` command line.

### GNU/Linux

Install RVGL by running the `setup` script included with the game. The setup converts 
all game files to lower case, adds necessary executable permissions, and installs an 
application launcher to `~/.local/share/applications`.

The game itself is provided as a launch script that auto-detects the system type and 
launches the appropriate (32-bit or 64-bit) executable. The script attempts to 
automatically resolve dependencies by using libs included with RVGL in place of any 
missing libs. This allows the game to run without any manual configuration on a wide 
range of systems as long as the OpenGL drivers, SDL2 and SDL2_image are installed.

All game files need to be in *lower case*. To repair file names after installing custom 
content, run the `fix_cases` script. Starting from version `18.1020a`, these scripts 
are written to target Bash 4.

#### Required Packages

```bash
# Debian / Ubuntu
sudo apt install libsdl2-2.0-0 libsdl2-image-2.0-0
sudo apt install libopenal1 libenet7 libunistring2
sudo apt install libjpeg8 libpng16-16 libtiff5 libwebp6
sudo apt install libvorbisfile3 libflac8 libmpg123-0 libfluidsynth1

# Arch Linux
sudo pacman -S sdl2 sdl2_image
sudo pacman -S openal enet libunistring
sudo pacman -S libjpeg libpng libtiff libwebp
sudo pacman -S libvorbis flac mpg123 fluidsynth

# Fedora
sudo yum install SDL2 SDL2_image
sudo yum install openal enet libunistring
sudo yum install libjpeg libpng libtiff libwebp
sudo yum install libvorbis flac mpg123 fluidsynth
```

#### Discord URI

RVGL registers the custom URI `discord-472158403830218762://` for Discord Rich Presence 
integration. The registration is done only once during the RVGL setup. To customize the 
URI, eg., to set various launch parameters, edit the Desktop entry at:
```
~/.local/share/applications/discord-472158403830218762.desktop
```
The `"%u"` at the end of the path is a required parameter. The default URI can be 
re-registered by running RVGL with the `-register` command line.

### Android

#### Locating the data path  

The following locations are supported:
```
/sdcard/RVGL
/storage/sdcard0/RVGL
/storage/sdcard1/RVGL
/storage/emulated/legacy/RVGL
/storage/emulated/0/RVGL
/storage/emulated/1/RVGL
/storage/extSdCard/RVGL
```

> If you already ran the app, it should have automatically generated an RVGL 
folder for you with updated assets. In this case, extract the full game data into 
this folder, taking care *not* to overwrite any of the existing files (i.e., say 
NO to replace).

#### Generated files  

+ The log file is generated at *profiles/re-volt_log.txt*.  
+ A file named *rvgl_version.txt* is generated on first run. Deleting it 
    will force the game to re-extract the latest assets from the apk.  
+ Running the Controller Map app (see below) saves controller mappings to 
    *profiles/gamecontrollerdb.txt*.  
+ To display the frame rate, edit *rvgl.ini* and set *ShowFPS* to 1.  
+ To set the device orientation, edit *rvgl.ini* and set *Orientation* 
    to a value between 0-5.  

#### Improving performance  

The Shader Edition might benefit from level world (\*.w) files optimized with 
[WorldCut](http://jigebren.free.fr/games/pc/re-volt/#worldcut) to have larger 
sized meshes. These can be downloaded from 
[here](http://rvgl.re-volt.io/downloads/rvgl_world_optimized.7z). As of version 
`18.1126a`, optimized level files are included in the app package.

#### Gameplay tips  

+ *Flip Car*: When the car is upside down, tap anywhere at the centre of the 
    screen (eg., on the car itself) to flip the car.  
+ *UI navigation*: Tap anywhere outside the menu box, or use the Back button on 
    the device, to go back to the previous screen.  

#### Native Multisampling

Native multisampling is very fast on embedded GPUs and enjoys wider support:

- 2x or 4x native multisampling can be enabled on embedded GPUs with virtually no performance loss.
- It's supported on low end GLES2 hardware like the Adreno 203.
- It can be used with either the shader-based GLES2+ renderer or the legacy GLES1 renderer.

However, native multisampling cannot be configured in-game from Render Settings. It must be set 
directly in *rvgl.ini* through the *Antialias* key. It's usually safe to set it to 2 or 4. The 
behavior is undefined when an unsupported value is used. The currently used Antialias level 
can still be checked from Render Settings.

The alternative is to use Frame Buffer Objects for multisampling, which can be quite slow on 
embedded GPUs. Because of the performance benefits, FBO is disabled by default on Android.

#### Controller Map

Available as a separate optional download, this app allows you to generate SDL *GameController* 
mappings for your controllers interactively. Once configured, the mappings are automatically 
saved to *profiles/gamecontrollerdb.txt* and is used by the game upon next start.

#### Command Lines

As of version `20.0210a`, it is possible to launch the app from adb shell with optional 
command line arguments, set through a string extra called `args`.
```
am start -n com.rvgl.rvgl/.RVGLActivity -e args "-nointro -lobby 1.1.1.1"
```
