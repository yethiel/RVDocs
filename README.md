# RVGL Docs

## Requirements

- pandoc


## Folders

`src`: The source chapters (markdown files).
`assets`: All contents of this folder will be copied to the public folder.
`public`: The resulting document.

## Generating the documentation

Run `build_html.sh`.
