#!/bin/sh

# Updates the header
# git submodule foreach git pull origin master

# Creates asset dir
if [ ! -d "assets" ]; then
    mkdir assets
fi

# Clears public directory
if [ -d "public" ]; then
    rm -r public
fi
mkdir public

# Merges all documents and creates a table of contents
pandoc -s -c pandoc.css --toc --toc-depth=3 src/* -o "public/index_temp.html"

# Adds the rvio header into the document
sed  -i '/<link/a <link href="rvio-header.css" rel="stylesheet" />' public/index_temp.html >> public/index_temp.html
sed  -e '/<header/r rvio-header/rvio-header.html' -e 'x;$G' public/index_temp.html >> public/index.html
rm public/index_temp.html

# Copies all assets into the public dir
cp rvio-header/rvio-header.css public/
cp -r assets/* public/
