{
;==============================================================================
;  RVGL FRONTEND PROPERTIES
;
;  This file allows modification of these frontend properties:
;  - Camera positions
;==============================================================================


;==============================================================================
;  CAMERA POSITIONS
;==============================================================================
;  Default camera IDs:
;    INIT:          0  |  NAME_SELECT:       12
;    START:         1  |  DINKYDERBY:        13
;    CAR_SELECT:    2  |  RACE:              14
;    CAR_SELECTED:  3  |  BESTTIMES:         15
;    TRACK_SELECT:  4  |  INTO_TRACKSCREEN:  16
;    MULTI:         5  |  SUMMARY:           17
;    TROPHY1:       6  |  PODIUMSTART:       18
;    TROPHY2:       7  |  PODIUM:            19
;    TROPHY3:       8  |  PODIUMVIEW1:       20
;    TROPHY4:       9  |  PODIUMVIEW2:       21
;    TROPHYALL:    10  |  PODIUMVIEW3:       22
;    OVERVIEW:     11  |  PODIUMLOSE:        23
;==============================================================================
;==============================================================================

CAMPOS {
  ID     0                                      ; Camera to replace [0 - 23]
  Name   "INIT"                                 ; Display name

  Eye    76.500000 -79.300000 -4462.600000      ; Camera position
  Focus  109.500000 -155.300000 -3466.100000    ; Camera focus
}

CAMPOS {
  ID     1                                      ; Camera to replace [0 - 23]
  Name   "START"                                ; Display name

  Eye    93.500000 -102.900000 -1926.700000     ; Camera position
  Focus  110.900000 -296.562000 -945.700000     ; Camera focus
}

CAMPOS {
  ID     2                                      ; Camera to replace [0 - 23]
  Name   "CAR_SELECT"                           ; Display name

  Eye    863.500000 -1092.500000 512.500000     ; Camera position
  Focus  1491.200000 -561.900000 1082.200000    ; Camera focus
}

CAMPOS {
  ID     3                                      ; Camera to replace [0 - 23]
  Name   "CAR_SELECTED"                         ; Display name

  Eye    1355.500000 -166.200000 864.400000     ; Camera position
  Focus  2204.600000 360.000000 911.500000      ; Camera focus
}

CAMPOS {
  ID     4                                      ; Camera to replace [0 - 23]
  Name   "TRACK_SELECT"                         ; Display name

  Eye    1589.600000 -81.800000 -1743.800000    ; Camera position
  Focus  2445.700000 -365.500000 -1311.800000   ; Camera focus
}

CAMPOS {
  ID     5                                      ; Camera to replace [0 - 23]
  Name   "MULTI"                                ; Display name

  Eye    -212.500000 -664.600000 -706.800000    ; Camera position
  Focus  -212.500000 -901.300000 134.400000     ; Camera focus
}

CAMPOS {
  ID     6                                      ; Camera to replace [0 - 23]
  Name   "TROPHY1"                              ; Display name

  Eye    -1773.900000 -503.900000 -477.200000   ; Camera position
  Focus  -2657.600000 -41.100000 -546.700000    ; Camera focus
}

CAMPOS {
  ID     7                                      ; Camera to replace [0 - 23]
  Name   "TROPHY2"                              ; Display name

  Eye    -1637.200000 -764.600000 206.400000    ; Camera position
  Focus  -2420.900000 -224.800000 -101.000000   ; Camera focus
}

CAMPOS {
  ID     8                                      ; Camera to replace [0 - 23]
  Name   "TROPHY3"                              ; Display name

  Eye    -1855.200000 -733.400000 669.900000    ; Camera position
  Focus  -2503.000000 -83.400000 271.300000     ; Camera focus
}

CAMPOS {
  ID     9                                      ; Camera to replace [0 - 23]
  Name   "TROPHY4"                              ; Display name

  Eye    -2007.700000 -713.500000 1026.900000   ; Camera position
  Focus  -2575.500000 -29.300000 569.300000     ; Camera focus
}

CAMPOS {
  ID     10                                      ; Camera to replace [0 - 23]
  Name   "TROPHYALL"                             ; Display name

  Eye    -1171.600000 -137.800000 834.400000     ; Camera position
  Focus  -2080.100000 -287.100000 444.100000     ; Camera focus
}

CAMPOS {
  ID     11                                      ; Camera to replace [0 - 23]
  Name   "OVERVIEW"                              ; Display name

  Eye    -816.200000 -534.500000 -500.000000     ; Camera position
  Focus  -1591.000000 -392.400000 -1115.900000   ; Camera focus
}

CAMPOS {
  ID     12                                      ; Camera to replace [0 - 23]
  Name   "NAME_SELECT"                           ; Display name

  Eye    527.100000 -930.500000 1322.300000      ; Camera position
  Focus  1105.500000 -375.500000 1922.100000     ; Camera focus
}

CAMPOS {
  ID     13                                      ; Camera to replace [0 - 23]
  Name   "DINKYDERBY"                            ; Display name

  Eye    -521.200000 -2870.000000 -325.400000    ; Camera position
  Focus  -306.200000 -1894.600000 -276.300000    ; Camera focus
}

CAMPOS {
  ID     14                                      ; Camera to replace [0 - 23]
  Name   "RACE"                                  ; Display name

  Eye    146.400000 -919.200000 -206.200000      ; Camera position
  Focus  35.600000 33.000000 -78.300000          ; Camera focus
}

CAMPOS {
  ID     15                                      ; Camera to replace [0 - 23]
  Name   "BESTTIMES"                             ; Display name

  Eye    -696.800000 -493.200000 -1728.400000    ; Camera position
  Focus  -1611.200000 -510.200000 -2133.000000   ; Camera focus
}

CAMPOS {
  ID     16                                      ; Camera to replace [0 - 23]
  Name   "INTO_TRACKSCREEN"                      ; Display name

  Eye    2233.900000 -507.600000 -1393.000000    ; Camera position
  Focus  3125.800000 -664.100000 -968.600000     ; Camera focus
}

CAMPOS {
  ID     17                                      ; Camera to replace [0 - 23]
  Name   "SUMMARY"                               ; Display name

  Eye    2026.200000 -407.200000 -1501.500000    ; Camera position
  Focus  2914.300000 -579.500000 -1075.500000    ; Camera focus
}

CAMPOS {
  ID     18                                      ; Camera to replace [0 - 23]
  Name   "PODIUMSTART"                           ; Display name

  Eye    -1047.100000 -746.200000 1165.800000    ; Camera position
  Focus  -1692.100000 -377.200000 1835.100000    ; Camera focus
}

CAMPOS {
  ID     19                                      ; Camera to replace [0 - 23]
  Name   "PODIUM"                                ; Display name

  Eye    -1047.100000 -746.200000 1165.800000    ; Camera position
  Focus  -1692.100000 -377.200000 1835.100000    ; Camera focus
}

CAMPOS {
  ID     20                                      ; Camera to replace [0 - 23]
  Name   "PODIUMVIEW1"                           ; Display name

  Eye    -1436.800000 -612.400000 1353.800000    ; Camera position
  Focus  -1906.300000 -354.400000 2198.200000    ; Camera focus
}

CAMPOS {
  ID     21                                      ; Camera to replace [0 - 23]
  Name   "PODIUMVIEW2"                           ; Display name

  Eye    -1676.000000 -534.900000 1268.100000    ; Camera position
  Focus  -1950.700000 -21.600000 2081.100000     ; Camera focus
}

CAMPOS {
  ID     22                                      ; Camera to replace [0 - 23]
  Name   "PODIUMVIEW3"                           ; Display name

  Eye    -1052.300000 -437.100000 1770.100000    ; Camera position
  Focus  -1926.200000 -44.500000 2056.700000     ; Camera focus
}

CAMPOS {
  ID     23                                      ; Camera to replace [0 - 23]
  Name   "PODIUMLOSE"                            ; Display name

  Eye    -1047.100000 -746.200000 1165.800000    ; Camera position
  Focus  -1692.100000 -0.000000 1835.100000      ; Camera focus
}

}
