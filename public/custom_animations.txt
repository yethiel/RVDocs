{
;==============================================================================
;  RVGL CUSTOM ANIMATIONS
;
;  This file allows creating custom object animation (moving whole meshes):
;  - Up to 256 different animations can be defined.
;  - Each has a main body part and up to 15 child bones which follow their parent bone.
;  - Every animation can have up to 256 keyframes, defining the bone positions at that time.
;    The game then interpolates the animation between those keyframes.
;  - Up to 64 custom model slots are supported.
;
;  The type of interpolation can be one of the following:
;    0: linear. Constant velocity and angular velocity.
;    1: smooth start, sudden end. Like an object starting to fall, then hitting the ground.
;    2: sudden start, smooth end.
;    3: both smooth start and end.
;    4: exaggerating or overshooting, i.e. first moving a bit in the opposite direction,
;       then over the target amount, then again backwards. Gives a robotic look.
;==============================================================================

;==============================================================================
;  ANIMATION
;==============================================================================
;  Possible options that can be defined within an animation block, and their 
;  default values are given below:
;
;  ANIMATION {
;    Slot                      0                         ; Slot ID (0 to 255)
;    Name                      "DEFAULT"                 ; Display name (currently unused)
;    Mode                      0                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)
;    NeedsTrigger              false                     ; Whether the animation needs to be triggered
;
;    BONE {
;      BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
;      ModelID                 -1                        ; Model ID (0 to 63, or -1)
;      Parent                  0                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
;      OffsetTranslation       0.000 0.000 0.000         ; Offset to parent location
;      OffsetRotationAxis      0.000 -1.000 0.000        ; Offset rotation axis
;      OffsetRotationAmount    0.000                     ; Offset rotation (in degrees)
;    }
;
;    KEYFRAME {
;      FrameNr                 0                         ; Number of the keyframe (0 to 255)
;      Time                    1.000                     ; Time (in seconds) since previous keyframe
;      Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
;      BONE {
;        BoneID                0                         ; Body part ID
;        Visible               true                      ; Toggle body part visibility (for all future keyframes until it is changed again)
;        Translation           0.000 0.000 0.000         ; Translation
;        RotationAxis          0.000 -1.000 0.000        ; Rotation axis
;        RotationAmount        0.000                     ; Rotation (in degrees)
;      }
;    }
;  }
;==============================================================================
;==============================================================================

;--------
; Models
;--------

MODEL   0   "tw_plane"
MODEL   1   "tw_plane_propellor"
MODEL   2   "tw_train"
MODEL   3   "tw_train_fwheel"
MODEL   4   "tw_train_rwheel"
MODEL   5   "mk_thwomp"
MODEL   6   "muse_barrel"
MODEL   7   "muse_mercury"
MODEL   8   "muse_venus"
MODEL   9   "muse_earth"
MODEL   10  "muse_moon"
MODEL   11  "mhk_robot_base"
MODEL   12  "mhk_robot_shoulder"
MODEL   13  "mhk_robot_upper_arm"
MODEL   14  "mhk_robot_elbow"
MODEL   15  "mhk_robot_lower_arm"
MODEL   16  "mhk_robot_hand"

;----------------------------
; Example 1: Toy World Plane
;----------------------------

ANIMATION {
  Slot                      0                         ; Slot ID (0 to 255)
  Name                      "Toy World Plane"         ; Display name (currently unused)
  Mode                      0                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)

  BONE {
    BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 -1                        ; Model ID (0 to 63, or -1)
  }

  BONE {
    BoneID                  1                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 0                         ; Model ID (0 to 63, or -1)
    Parent                  0                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       500.000 0.000 0.000       ; Offset to parent location
    OffsetRotationAxis      0.000 0.000 1.000         ; Offset rotation axis
    OffsetRotationAmount    30.000                    ; Offset rotation (in degrees)
  }

  BONE {
    BoneID                  2                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 1                         ; Model ID (0 to 63, or -1)
    Parent                  1                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       0.000 -120.000 110.000    ; Offset to parent location
  }

  KEYFRAME {
    FrameNr                 0                         ; Number of the keyframe (0 to 255)
    Time                    1.500                     ; Time (in seconds) since previous keyframe
    Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                0                         ; Body part ID
      RotationAxis          0.000 1.000 0.000         ; Rotation axis
      RotationAmount        360.000                   ; Rotation (in degrees)
    }
    BONE {
      BoneID                1                         ; Body part ID
    }
    BONE {
      BoneID                2                         ; Body part ID
      RotationAxis          0.000 0.000 1.000         ; Rotation axis
      RotationAmount        3600.000                  ; Rotation (in degrees)
    }
  }
}

;----------------------------
; Example 2: Toy World Train
;----------------------------

ANIMATION {
  Slot                      1                         ; Slot ID (0 to 255)
  Name                      "Toy World Train"         ; Display name (currently unused)
  Mode                      0                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)

  BONE {
    BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 2                         ; Model ID (0 to 63, or -1)
  }

  BONE {
    BoneID                  1                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 3                         ; Model ID (0 to 63, or -1)
    Parent                  0                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       98.000 -136.000 330.000   ; Offset to parent location
  }

  BONE {
    BoneID                  2                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 3                         ; Model ID (0 to 63, or -1)
    Parent                  0                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       -98.000 -136.000 330.000  ; Offset to parent location
  }

  BONE {
    BoneID                  3                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 4                         ; Model ID (0 to 63, or -1)
    Parent                  0                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       98.000 -92.000 -204.000   ; Offset to parent location
  }

  BONE {
    BoneID                  4                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 4                         ; Model ID (0 to 63, or -1)
    Parent                  0                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       -98.000 -92.000 -204.000  ; Offset to parent location
  }

  KEYFRAME {
    FrameNr                 0                         ; Number of the keyframe (0 to 255)
    Time                    20.000                    ; Time (in seconds) since previous keyframe
    Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                0                         ; Body part ID
      Translation           0.000 0.000 -4000.000     ; Translation
    }
    BONE {
      BoneID                1                         ; Body part ID
      RotationAxis          1.000 0.000 0.000         ; Rotation axis
      RotationAmount        -1500.000                 ; Rotation (in degrees)
    }
    BONE {
      BoneID                2                         ; Body part ID
      RotationAxis          1.000 0.000 0.000         ; Rotation axis
      RotationAmount        -1500.000                 ; Rotation (in degrees)
    }
    BONE {
      BoneID                3                         ; Body part ID
      RotationAxis          1.000 0.000 0.000         ; Rotation axis
      RotationAmount        -2500.000                 ; Rotation (in degrees)
    }
    BONE {
      BoneID                4                         ; Body part ID
      RotationAxis          1.000 0.000 0.000         ; Rotation axis
      RotationAmount        -2500.000                 ; Rotation (in degrees)
    }
  }
}

;------------------------------
; Example 3: Mario Kart Thwomp
;------------------------------

ANIMATION {
  Slot                      2                         ; Slot ID (0 to 255)
  Name                      "Mario Kart Thwomp"       ; Display name (currently unused)
  Mode                      0                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)
  NeedsTrigger              false                     ; whether it has to be triggered to start

  BONE {
    BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 5                         ; Model ID (0 to 63, or -1)
    OffsetTranslation       0.000 -500.000 0.000      ; Initial offset
  }

  KEYFRAME {
    FrameNr                 0                         ; Number of the keyframe (0 to 255)
    Time                    0.400                     ; Time (in seconds) since previous keyframe
    Type                    1                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                0                         ; Body part ID
      Translation           0.000 500.000 0.000       ; Translation
    }
  }

  KEYFRAME {
    FrameNr                 1                         ; Number of the keyframe (0 to 255)
    Time                    1.000                     ; Time (in seconds) since previous keyframe
    Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
  }

  KEYFRAME {
    FrameNr                 2                         ; Number of the keyframe (0 to 255)
    Time                    4.000                     ; Time (in seconds) since previous keyframe
    Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                0                         ; Body part ID
      Translation           0.000 -500.000 0.000      ; Translation
    }
  }

  KEYFRAME {
    FrameNr                 3                         ; Number of the keyframe (0 to 255)
    Time                    4.600                     ; Time (in seconds) since previous keyframe
    Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
  }
}

;--------------------------
; Example 4: Museum Barrel
;--------------------------

ANIMATION {
  Slot                      3                         ; Slot ID (0 to 255)
  Name                      "Museum Barrel"           ; Display name (currently unused)
  Mode                      0                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)

  BONE {
    BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 6                         ; Model ID (0 to 63, or -1)
  }

  KEYFRAME {
    FrameNr                 0                         ; Number of the keyframe (0 to 255)
    Time                    10.000                    ; Time (in seconds) since previous keyframe
    Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                0                         ; Body part ID
      RotationAxis          1.000 0.000 0.000         ; Rotation axis
      RotationAmount        360.000                   ; Rotation (in degrees)
    }
  }
}

;--------------------------------
; Example 5: Museum Solar System
; Mercury, Venus, Earth + Moon
;--------------------------------

ANIMATION {
  Slot                      4                         ; Slot ID (0 to 255)
  Name                      "Museum Solar System A"   ; Display name (currently unused)
  Mode                      0                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)

  BONE {
    BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 -1                        ; Model ID (0 to 63, or -1)
  }
  ; mercury
  BONE {
    BoneID                  1                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 -1                        ; Model ID (0 to 63, or -1)
    Parent                  0                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
  }
  BONE {
    BoneID                  2                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 7                         ; Model ID (0 to 63, or -1)
    Parent                  1                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       800.000 0.000 0.000       ; Initial offset
  }
  ; venus
  BONE {
    BoneID                  3                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 -1                        ; Model ID (0 to 63, or -1)
    Parent                  0                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
  }
  BONE {
    BoneID                  4                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 8                         ; Model ID (0 to 63, or -1)
    Parent                  3                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       1400.000 0.000 0.000      ; Initial offset
  }
  ; earth + moon
  BONE {
    BoneID                  5                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 -1                        ; Model ID (0 to 63, or -1)
    Parent                  0                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
  }
  BONE {
    BoneID                  6                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 9                         ; Model ID (0 to 63, or -1)
    Parent                  5                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       2000.000 0.000 0.000      ; Initial offset
  }
  BONE {
    BoneID                  7                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 10                        ; Model ID (0 to 63, or -1)
    Parent                  6                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       300.000 0.000 0.000       ; Offset to parent location
  }

  KEYFRAME {
    FrameNr                 0                         ; Number of the keyframe (0 to 255)
    Time                    180.000                   ; Time (in seconds) since previous keyframe
    Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                0                         ; Body part ID
    }
    ; mercury
    BONE {
      BoneID                1                         ; Body part ID
      RotationAxis          0.000 1.000 1.000         ; Rotation axis
      RotationAmount        3240.000                  ; Rotation (in degrees)
    }
    BONE {
      BoneID                2                         ; Body part ID
      RotationAxis          0.000 1.000 0.000         ; Rotation axis
      RotationAmount        7200.000                  ; Rotation (in degrees)
    }
    ; venus
    BONE {
      BoneID                3                         ; Body part ID
      RotationAxis          0.000 1.000 1.000         ; Rotation axis
      RotationAmount        2880.000                  ; Rotation (in degrees)
    }
    BONE {
      BoneID                4                         ; Body part ID
      RotationAxis          0.000 1.000 0.000         ; Rotation axis
      RotationAmount        7200.000                  ; Rotation (in degrees)
    }
    ; earth + moon
    BONE {
      BoneID                5                         ; Body part ID
      RotationAxis          0.000 1.000 1.000         ; Rotation axis
      RotationAmount        2520.000                  ; Rotation (in degrees)
    }
    BONE {
      BoneID                6                         ; Body part ID
      RotationAxis          0.000 1.000 0.000         ; Rotation axis
      RotationAmount        7200.000                  ; Rotation (in degrees)
    }
    BONE {
      BoneID                7                         ; Body part ID
      RotationAxis          0.000 1.000 1.000         ; Rotation axis
      RotationAmount        -8000.000                 ; Rotation (in degrees)
    }
  }
}

;--------------------------
; Example 6: Factory Robot
;--------------------------

ANIMATION {
  Slot                      5                         ; Slot ID (0 to 255)
  Name                      "Factory Robot"           ; Display name (currently unused)
  Mode                      2                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)

  BONE {
    BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 11                        ; Model ID (0 to 63, or -1)
  }
  BONE {
    BoneID                  1                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 12                        ; Model ID (0 to 63, or -1)
    Parent                  0                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       0.000 -80.000 0.000       ; Initial offset
    OffsetRotationAxis      0.000 0.000 1.000         ; Offset rotation axis
    OffsetRotationAmount    10.000                    ; Offset rotation (in degrees)
  }
  BONE {
    BoneID                  2                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 13                        ; Model ID (0 to 63, or -1)
    Parent                  1                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       0.000 -200.000 0.000      ; Initial offset
    OffsetRotationAxis      0.000 0.000 1.000         ; Offset rotation axis
    OffsetRotationAmount    50.000                    ; Offset rotation (in degrees)
  }
  BONE {
    BoneID                  3                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 14                        ; Model ID (0 to 63, or -1)
    Parent                  2                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       310.000 0.000 0.000       ; Initial offset
    OffsetRotationAxis      0.000 0.000 1.000         ; Offset rotation axis
    OffsetRotationAmount    -50.000                   ; Offset rotation (in degrees)
  }
  BONE {
    BoneID                  4                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 15                        ; Model ID (0 to 63, or -1)
    Parent                  3                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       230.000 0.000 0.000       ; Initial offset
    OffsetRotationAxis      0.000 0.000 1.000         ; Offset rotation axis
    OffsetRotationAmount    -60.000                   ; Offset rotation (in degrees)
  }
  BONE {
    BoneID                  5                         ; Body part ID (0 to 15); part 0 is the main body part
    ModelID                 16                        ; Model ID (0 to 63, or -1)
    Parent                  4                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
    OffsetTranslation       310.000 5.000 0.000       ; Initial offset
    OffsetRotationAxis      0.000 0.000 1.000         ; Offset rotation axis
    OffsetRotationAmount    -40.000                   ; Offset rotation (in degrees)
  }

  KEYFRAME {
    FrameNr                 0                         ; Number of the keyframe (0 to 255)
    Time                    1.000                     ; Time (in seconds) since previous keyframe
    Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
  }

  KEYFRAME {
    FrameNr                 1                         ; Number of the keyframe (0 to 255)
    Time                    0.400                     ; Time (in seconds) since previous keyframe
    Type                    3                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                1                         ; Body part ID
      RotationAxis          0.000 0.000 1.000         ; Rotation axis
      RotationAmount        10.000                    ; Rotation (in degrees)
    }
    BONE {
      BoneID                3                         ; Body part ID
      RotationAxis          0.000 0.000 1.000         ; Rotation axis
      RotationAmount        -10.000                   ; Rotation (in degrees)
    }
    BONE {
      BoneID                4                         ; Body part ID
      RotationAxis          0.000 0.000 1.000         ; Rotation axis
      RotationAmount        10.000                    ; Rotation (in degrees)
    }
    BONE {
      BoneID                5                         ; Body part ID
      RotationAxis          0.000 0.000 1.000         ; Rotation axis
      RotationAmount        -10.000                   ; Rotation (in degrees)
    }
  }

  KEYFRAME {
    FrameNr                 2                         ; Number of the keyframe (0 to 255)
    Time                    0.800                     ; Time (in seconds) since previous keyframe
    Type                    3                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                0                         ; Body part ID
      RotationAxis          0.000 1.000 0.000         ; Rotation axis
      RotationAmount        -180.000                  ; Rotation (in degrees)
    }
    BONE {
      BoneID                1                         ; Body part ID
      RotationAxis          0.000 0.000 1.000         ; Rotation axis
      RotationAmount        -35.000                   ; Rotation (in degrees)
    }
    BONE {
      BoneID                2                         ; Body part ID
      RotationAxis          0.000 0.000 1.000         ; Rotation axis
      RotationAmount        20.000                    ; Rotation (in degrees)
    }
    BONE {
      BoneID                3                         ; Body part ID
      RotationAxis          0.000 0.000 1.000         ; Rotation axis
      RotationAmount        25.000                    ; Rotation (in degrees)
    }
    BONE {
      BoneID                4                         ; Body part ID
      RotationAxis          0.000 0.000 1.000         ; Rotation axis
      RotationAmount        35.000                    ; Rotation (in degrees)
    }
    BONE {
      BoneID                5                         ; Body part ID
      RotationAxis          0.000 0.000 1.000         ; Rotation axis
      RotationAmount        35.000                    ; Rotation (in degrees)
    }
  }

  KEYFRAME {
    FrameNr                 3                         ; Number of the keyframe (0 to 255)
    Time                    0.200                     ; Time (in seconds) since previous keyframe
    Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
  }

  KEYFRAME {
    FrameNr                 4                         ; Number of the keyframe (0 to 255)
    Time                    1.000                     ; Time (in seconds) since previous keyframe
    Type                    3                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
    BONE {
      BoneID                5                         ; Body part ID
      RotationAxis          1.000 0.000 0.000         ; Rotation axis
      RotationAmount        980.000                   ; Rotation (in degrees)
    }
  }

  KEYFRAME {
    FrameNr                 5                         ; Number of the keyframe (0 to 255)
    Time                    0.000                     ; Time (in seconds) since previous keyframe
    Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
  }
}

}
